This is a simple Arduino project to receive and/or broadcast 315MHz or 433MHz signals via commands from a serial connection - a simple bridge between radio and serial.

It was designed to connect directly to a Raspberry Pi, announce when new 433MHz signals are detected, and broadcast signals when told. The bridge should work with any device capable of serial communication.

It can be used for basic home automation to control radio appliance sockets etc, and remote control of the computer via an RF remote control or keyfob.

Almost any proprietary radio system will do, I picked up some RF-controlled sockets for about $10 at my local supermarket.

> NOTE: If you're looking to simply control RF devices with a RaspberryPi, there are alternatives which don't require an Arduino. I did it this way so that any serial-connectable controller will work, and to avoid timing issues with complex RF sequences on a non-realtime operating system. Most people don't have this problem!


Do you have a spare RF remote control? Why not receive it and set up...

* A remote with macros for your favorite apps/tasks
* Buttons to send wake-on-LAN packets
* A remote button to enable/disable guest WiFi
* A remote to set speed limits on your home seedbox

Do you have spare RF-controlled sockets? Why not transmit and set up...

* Heated grow beds which turn on when its about to get frosty
* CCTV monitor that turns on when motion is detected
* Lights which turn off when VLC/Kodi/etc starts playing
* Bedside pressure mat controlled coffee machine
* A lamp controlled by a public web page, what's the worst that could happen?
* Laundry killswitch when the machine gets a bit fiesty

Or other 315/433MHz devices?

* Open the garage door when your car's MAC address appears on the WiFi
* Dispense air freshener while your girlfriend's MAC address is on your WiFi
* Loud dog noises when the doorbell is pressed

None of the above?

* Impress your friends with simple replay attacks
* Scare your neighbors with replay+1 car keyfob attacks
* Learn to control street lamps in urban areas
* Secretly adjust the air conditioning in the office


# Getting started

![breadboard](https://i.imgur.com/9sbTC8a.png)

## Hardware

* Arduino Uno or other development board, or any atmega328u circuit
* Cheap 433MHz transmitter and/or receiver unit(s)
* 2x LEDs for receive/transmit notification
* 2x 220R current limiting resistors for LEDs
* 1k5 and 3k3 resistors for serial line level

Most atmel chips will do the job, I just used what I had on hand. This could easily be converted to other frequencies, infrared, etc.


### Connect radios

This is straightforward.

1. Wire the radio board(s) to +5v and GND from the Raspberry Pi.
1. The radio receiver data pin should connect to an Arduino interrupt pin. The default is interrupt 0 / digital pin #2 / physical pin 4.
1. The radio transmitter should connect to a PWM digital output pin. The default is digital pin #10 / physical pin 16


### Connect Raspberry Pi

The Raspberry Pi serial connection operates at 3.3v but the Arduino serial transmit line is at 5v. We make a simple voltage divider with two resistors, but a logic line level converter would be better. This would be my first upgrade.

1. Wire the RPi serial TX pin to the Arduino serial RX digital pin #0 / physical pin 2.
1. Wire the RPi serial RX pin via a 1k5 resistor to the Arduino serial TX digital pin #1 / physical pin 3.
1. Wire the 3k3 resistor from Arduino serial TX to GND to complete the voltage divider


### Optional feedback LEDs

These are optional but useful for debugging.

1. Connect a receivery-colored LED +ve to digital pin 8 / physical pin 14
1. Connect a transmitty-colored LED +ve to digital pin 9 / physical pin 15
1. Add 220R resistors to each LED -ve to GND


## Software

First install the [rc-switch library](https://github.com/sui77/rc-switch/) into your Arduino IDE or build toolchain and restart if necessary.

Now download this project, open the sketch and compile/upload as normal.

If you're using an Arduino development board like the Arduino Uno, without any connections, you'll notice the serial TX light blink when you transmit on 433MHz with a remote control. This is working!

Use the serial console in your Arduino IDE at 9600 baud and make a transmission. You should see something like this:

```
(1,275,32,1143271849)
(1,275,32,1143271849)
```

You've just received packets on protocol 1, with a pulse width of 275ms, a 32bit data packet and the value itself.

Now send that string back to the Arduino from the serial console. It will transmit the same data.


## Configuration

There are some settings variables at the top of the sketch which you can modify:

| Variable | Default value | Description |
|---|---|---|
| `serialBaud` | 9600 | Baud rate of the serial connection |
| `radioRxInt` | 0 | Interrupt pin connected to the radio receiver board |
| `radioTxPin` | 10 | Digital pin connected to the radio transmitter board |
| `rxLedPin` | 8 | Digital pin connected to the receive LED |
| `rxLedPin` | 9 | Digital pin connected to the transmit LED |
| `serialStartMarker` | ( | Character used at the start of serial commands |
| `serialEndMarker` | ) | Character used at the end of serial commands |
| `serialDelimiter` | , | Character used between data fields in serial commands |


## Serial format

Data in and out of the serial port uses the following format:

```
(PROTOCOL,PULSE_WIDTH,BIT_LENGTH,PAYLOAD)
```

Where:

| Variable | Type | Description |
|---|---|---|
| `PROTOCOL` | unsigned integer | Protocol number, used by the rc-switch library |
| `PULSE_WIDTH` | unsigned integer | Length of broadcast bits in milliseconds (normally 200-500) |
| `BIT_LENGTH` | unsigned integer | Number of bits contained by the payload (normally 16 or 32) |
| `PAYLOAD` | unsigned long | The actual data received or to transmit |


You can change the start and end markers and field delimiter (see configuration above). Here are some examples:

```
(1,275,32,1143271849)
<1:275:32:1143271849>
[1|275|32|1143271849]
```

Set the bridge to whichever format best suits your host controller (aka your Raspberry Pi code).


# Notes

Don't forget to switch off the serial console on the Raspberry Pi (in `sudo raspi-config`), add `enable_uart=1` to your `/boot/config.txt` for RPi v3 boards, and grant your user(s) permissions by adding them to the appropriate group (`dialout` I think?).

Not all rc-switch protocols were implemented. It should be trivial to add support for tristate/switch/etc RF gear because the library already supports it.

If a homemade atmega circuit is proving unreliable, try adding a crystal to run the chip faster and improve timing accuracy.

Range is often a problem with cheap RF boards. A simple wire antenna or wire coil antenna can be used to improve it slightly. For long-range receive consider using an RTL-SDR setup, and for long-range transmit consider a device like the HackRF One.


# Warning!

![take the men out of the loop](https://i.imgur.com/WTljvbW.png)

Controlling the real world from software can be very dangerous. Last year my friend absolutely didn't set ablaze a huge dry storage warehouse with faulty LiPo charging balancers. It took 3 days and many fire crews to put it out. Luckily no one was hurt.

Spare a thought for him as you gleefully plug dangerous things into an Arduino.

What happens if faulty code leaves your device on for 72 hours straight? Or switches it on and off at 10kHz? Or turns on all your devices simultaneously? Are they fused? Are they fused enough? Don't leave it unattended until you're very confident.

Volts don't kill! Amps don't kill! You kill!