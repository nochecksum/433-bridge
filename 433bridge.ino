/**
 * Settings
 */

int serialBaud = 9600;
int radioRxInt = 0; // Interrupt 0 = digital pin 2
int radioTxPin = 10;

int rxLedPin = 8;
int txLedPin = 9;

// Character at the start of in/out serial messages
char serialStartMarker = '(';

// Character at the end of in/out serial messages
char serialEndMarker = ')';

// Delimiter between serial data fields
char serialDelimiter = ',';


/**
 * Dependencies
 */

// https://github.com/sui77/rc-switch
#include <RCSwitch.h>


/**
 * Internal globals
 */

// Flag if new serial data is available
boolean newSerialData = false;

// Characters to expect on serial line
const byte numChars = 32;

// Buffer of received characters
char receivedSerialChars[numChars];

// Copy of received buffer for string manipulation
char tempSerialChars[numChars];

// Radio library
RCSwitch radio = RCSwitch();


/**
 * setup()
 * Called once when the chip boots
 */

void setup() {
  // Start serial connection
  Serial.begin(serialBaud);

  // Start radio
  radio.enableReceive(radioRxInt);
  radio.enableTransmit(radioTxPin);

  // LEDs
  pinMode(rxLedPin, OUTPUT);
  pinMode(txLedPin, OUTPUT);
}


/**
 * loop()
 * Main program
 */

void loop() {
  // Check if radio data is available to be read
  radioRx();

  // Check if serial data is available
  serialRx();

  // If serial buffer has data ready
  if (newSerialData == true) {
    // Copy to temporary buffer for manipulating
    strcpy(tempSerialChars, receivedSerialChars);

    // Attempt to parse incoming data for command
    parseSerialData();

    // Reset flag
    newSerialData = false;
  }
}

/**
 * radioRx()
 * Receive and decode radio data
 */
void radioRx() {
  // Check if data is available
  if (radio.available()) {
    
    // RX LED on
    digitalWrite(rxLedPin, HIGH);
    
    // Fetch received information
    unsigned int protocol = radio.getReceivedProtocol();
    unsigned int delay = radio.getReceivedDelay();
    unsigned int bitLength = radio.getReceivedBitlength();
    unsigned long value = radio.getReceivedValue();

    // Send it over serial
    Serial.print(serialStartMarker);
    Serial.print(protocol);
    Serial.print(serialDelimiter);
    Serial.print(delay);
    Serial.print(serialDelimiter);
    Serial.print(bitLength);
    Serial.print(serialDelimiter);
    Serial.print(value);
    Serial.println(serialEndMarker);

    // Reset available flag
    radio.resetAvailable();

    // RX LED off
    digitalWrite(rxLedPin, LOW);
  }
}

void serialRx() {
    static boolean recvInProgress = false;
    static byte ndx = 0;
    char rc;

    // Read serial buffer if old buffer is empty
    while (Serial.available() > 0 && newSerialData == false) {
        // Get latest data
        rc = Serial.read();

        // Are we past the start of a message?
        if (recvInProgress == true) {
            // Are we not at the end of a message?
            if (rc != serialEndMarker) {
                // Append received bit to buffer
                receivedSerialChars[ndx] = rc;
                // Increment buffer counter
                ndx++;
                if (ndx >= numChars) {
                    ndx = numChars - 1;
                }
                
            // Must be the end of the message
            } else {
                // Terminate the buffer string
                receivedSerialChars[ndx] = '\0';
                // Reset variables
                recvInProgress = false;
                ndx = 0;
                
                // Buffer is ready to be processed flag
                newSerialData = true;
            }

        // Are we at the start of a message?
        } else if (rc == serialStartMarker) {
            // Begin reading stream
            recvInProgress = true;
        }
    }
}


/**
 * parseSerialData()
 * Transmits radio based on command string
 */

void parseSerialData() {
    char * strtokIndx; // this is used by strtok() as an index
    unsigned int protocol = 1;
    unsigned int pulseLength = 275;
    unsigned int bitLength = 32;
    unsigned long value = 0;

    // TX LED on
    digitalWrite(txLedPin, HIGH);
    
    // Protocol
    strtokIndx = strtok(tempSerialChars, &serialDelimiter);
    protocol = atoi(strtokIndx);

    // Delay
    strtokIndx = strtok(NULL, &serialDelimiter);
    pulseLength = atoi(strtokIndx);

    // Bitlength
    strtokIndx = strtok(NULL, &serialDelimiter);
    bitLength = atoi(strtokIndx);

    // Value
    strtokIndx = strtok(NULL, &serialDelimiter);
    value = atol(strtokIndx);

    // Transmit the data
    radio.setProtocol(protocol);
    radio.setPulseLength(pulseLength);
    radio.setRepeatTransmit(2);
    radio.send(value, bitLength);

    // TX LED off
    digitalWrite(txLedPin, LOW);

    // Delay to prevent TX "overlap"
    delay(500);
}
